from fastkml import kml
from dicttoxml import dicttoxml
from xml.dom.minidom import parseString

rawFolder = "./"
outputFoler = "../"
rawData = ["Dominica.kml", "Grenada.kml","StLucia.kml", "StVincent&Grenadines.kml"]
countryFolder = ['dominica', 'grenada', 'saintlucia', 'saintvincentgrenadines']


for i in range(0, len(countryFolder)):
    k = kml.KML()
    with open("{0}{1}".format(rawFolder, rawData[i])) as fp:
        print("Reading file: {0}".format( rawData[i] ))
        data = fp.read()
        fp.close()
    k.from_string(data)
    # print(k.to_string(prettyprint=True))
    document = list(k.features())[0]
    placemarks = list(document.features())
    print("{0} placemarks found".format(len(placemarks)))
    for placemark in placemarks:
        result = {}
        result['area'] = {}
        name = placemark.name
        print("Reading data for location: {0}".format(name) )
        result['area']['areaDesc'] = name
        result['area']['geocode'] = {}
        result['area']['geocode']['valuename'] = name

        polymer = ""
        for point in placemark.geometry.exterior.coords:
            polymer += "{0},{1} \n".format(point[0], point[1])
            # print("({0})".format(polymer))
        
        result['area']['polygon'] = polymer

        filename = "{0}{1}/{2}.xml".format(outputFoler, countryFolder[i], name.replace(" ", "_").rstrip())
        print("Create output file for {0} as {1}".format(countryFolder[i], filename))
        xml = dicttoxml(result, attr_type=False)
        dom = parseString(xml)
        with open(filename, "w") as outfp:
            outfp.write(dom.toprettyxml())
            outfp.close()
